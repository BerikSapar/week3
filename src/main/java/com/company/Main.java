package com.company;

import java.io.*;
import java.util.Scanner;

public class Main {


    public static void main(String[] args){
        try {
            start();
        } catch (FileNotFoundException e){
            System.out.println("Error: " + e.getMessage());
        }
    }

    private static Scanner sc = new Scanner(System.in);
    private static User signedUser;

    private static void addUser(User user) {
        register(user);
    }

    private static void menu() {
        while (true) {
            if (signedUser == null) {
                System.out.println("You are not signed in.");
                System.out.println("1. Sign In");
                System.out.println("2. Sign Up");
                System.out.println("3. Exit");
                int choice = sc.nextInt();
                if (choice == 1) signIn();
                else if (choice == 2) signUp();
                else break;
            } else {
                userProfile();
            }
        }
    }



    private static void userProfile() {
        signedUser = null;
    }

    private void logOff() {

    }

    private static void authentication(String login, String password) {
        try {
            Scanner in = new Scanner(new File("com/company/db.txt"));
            while (in.hasNextLine()) {
                String s = in.nextLine();
                String[] sArray = s.split(":");

                if (login.equals(sArray[0]) && password.equals(sArray[1])){
                    System.out.println("Successfully logged in");
                    System.exit(0);
                }
            }
            System.out.println("User not found");

            in.close();

        } catch (FileNotFoundException e) {
            System.out.println("Error: " + e.getMessage());
        }
    }

    private static void register(User user) {
        try {
            FileWriter write = new FileWriter("com/company/db.txt", true);
            BufferedWriter writer = new BufferedWriter(write);
            writer.write(user.toString());
            writer.newLine();
            writer.close();
            System.out.println("Successfully registered");
        } catch (IOException e) {
            System.out.println("Error: " + e.getMessage());
        }
    }

    private static void signIn() {
        System.out.println("----Sign In----");
        System.out.println("Login:");
        Scanner in = new Scanner(System.in);
        String login = in.nextLine();
        System.out.println("Password:");
        String password = in.nextLine();
        authentication(login, password);
    }

    private static void signUp() {
        System.out.println("----Sign Up----");
        System.out.println("Login:");
        Scanner in = new Scanner(System.in);
        String login = in.nextLine();
        System.out.println("Password:");
        String password = in.nextLine();
        System.out.println("Name:");
        String name = in.nextLine();
        System.out.println("Surname:");
        String surname = in.nextLine();

        User user = new User(name, surname, login);
        user.setPasswordStr(password);

        addUser(user);
    }

    public static void start() throws FileNotFoundException {
        while (true) {
            System.out.println("Welcome to my application!");
            System.out.println("Select command:");
            System.out.println("1. Menu");
            System.out.println("2. Exit");
            int choice = sc.nextInt();
            if (choice == 1) {
                menu();
            } else {
                break;
            }
        }
    }

    private void saveUserList() {

    }
}
