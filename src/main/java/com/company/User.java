package com.company;

public class User extends Password {
    private String name;
    private String surname;
    private String username;
    private String password;

    public User(String name, String surname, String username) {
        this.name = name;
        this.surname = surname;
        this.username = username;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }


    @Override
    public String getPasswordStr() {
        return super.getPasswordStr();
    }

    @Override
    public void setPasswordStr(String passwordStr) {
        this.password = passwordStr;
    }

    @Override
    public String toString() {
        return username+":"+password+":"+name+":"+surname;
    }
}
